﻿using System.Collections.Generic;
using Cyron43.GtaIV.Common;

namespace Autodrive
{
   public class ConfigurationContainer : IConfigurationContainer
   {
      public bool AllowRandomDriverModel { get; set; }
      public bool AlwaysDriveYourself { get; set; }
      public string DriverModel { get; set; }
      public List<KeyContainer> Keys { get; set; }
      
      /// <summary>
      /// For speeds of 45mph and above.
      /// </summary>
      public bool ObeyTrafficLawsOnHighSpeed { get; set; }

      /// <summary>
      /// For speeds up to 44mph.
      /// </summary>
      public bool ObeyTrafficLawsOnLowSpeed { get; set; }
      public bool StopOnStreet { get; set; }
      public int Version { get; set; }
   }
}