﻿using System;
using Cyron43.GtaIV.Common;
using GTA;

namespace Autodrive
{
   public class WalkHandling : Script
   {
      private readonly Core _core;
      private Vector3 _target;
      private WalkStyle _walkStyle;

      public WalkHandling(){}

      public WalkHandling(Core core)
      {
         _core = core;
         //Interval = 200;
      }

      internal bool AutoWalkRequested { get; set; }

      public void ToggleWalking()
      {
         if(!AutoWalkRequested)
         {
            _core.Tick -= StopIfTargetReached;
            _target = default(Vector3);
            Player.Character.Task.AlwaysKeepTask = false;
            Player.Character.Task.ClearAll();
            Player.Character.BlockPermanentEvents = false;
            CommonFunctions.DisplayText("Player character walking task has been released.", 3000);
            return;
         }
         _walkStyle = WalkStyle.Walk;
         var waypoint = Game.GetWaypoint();
         if(waypoint != null)
         {
            _target = waypoint.Position.ToGround();
            _core.Tick += StopIfTargetReached;
            OnFootToWaypoint();
         }
         else
            DoRandomWalk();
      }

      internal void SlowDown()
      {
         if(_target == default(Vector3))
            return;
         _walkStyle--;
         KeepWalkStyleInsideBoundaries();
         OnFootToWaypoint();
      }

      internal void SpeedUp()
      {
         if(_target == default(Vector3))
            return;
         _walkStyle++;
         KeepWalkStyleInsideBoundaries();
         OnFootToWaypoint();
      }

      private bool BetterIgnorePath()
      {
         if(_target.Z < 0 || Player.Character.Position.DistanceTo(_target) > 250)
         {
            CommonFunctions.DisplayText("No routed walking/swimming possible. Using a straight line.", 2500);
            return true;
         }
         return false;
      }

      private void DoRandomWalk()
      {
         CommonFunctions.DisplayText("Wandering around...", 3000);
         Player.Character.Task.WanderAround();
      }

      private void KeepWalkStyleInsideBoundaries()
      {
         if(_walkStyle < WalkStyle.Stop)
            _walkStyle = WalkStyle.Stop;
         if(_walkStyle > WalkStyle.Run)
            _walkStyle = WalkStyle.Run;
         Game.DisplayText("Walk style = " + _walkStyle, 2500);
      }

      private void OnFootToWaypoint()
      {
         CommonFunctions.DisplayText("On foot to waypoint...", 3000);
         Player.Character.Task.AlwaysKeepTask = false;
         Player.Character.Task.ClearAll();
         Game.LoadAllPathNodes = true;
         switch(_walkStyle)
         {
            case WalkStyle.Stop:
               Player.Character.Task.StandStill(-1);
               CommonFunctions.DisplayText("Character waits but is still on task.", 3000);
               break;
            case WalkStyle.Walk:
               if(BetterIgnorePath())
                  Player.Character.Task.GoTo(_target, IgnorePaths: true);
               else
                  Player.Character.Task.GoTo(_target, IgnorePaths: false);
               Player.Character.Task.AlwaysKeepTask = true;
               break;
            case WalkStyle.Run:
               if(BetterIgnorePath())
                  Player.Character.Task.RunTo(_target, IgnorePaths: true);
               else
                  Player.Character.Task.RunTo(_target, IgnorePaths: false);
               Player.Character.Task.AlwaysKeepTask = true;
               break;
         }
      }

      private void ReleaseFromTask()
      {
         _core.Tick -= StopIfTargetReached;
         _target = default(Vector3);
         AutoWalkRequested = false;
         ToggleWalking();
      }

      private void StopIfTargetReached(object sender, EventArgs eventArgs)
      {
         if((Player.Character.Position.DistanceTo2D(_target) > 3.0f))
            return;
         ReleaseFromTask();
      }
   }
}