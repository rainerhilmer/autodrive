namespace Autodrive
{
   internal enum SpeedState
   {
      Stop = 0,
      Patrol,
      Low,
      LowMed,
      Medium,
      High,
      Insane
   }
}