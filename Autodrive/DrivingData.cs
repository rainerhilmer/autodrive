﻿using GTA;

namespace Autodrive
{
   internal class DrivingData
   {
      internal bool AllowToDriveRoadsWrongWay { get; set; }
      internal Ped Driver { get; set; }
      internal bool DrivingIsActive { get; set; }
      internal string Message { get; set; }
      internal short DesiredAltitude { get; set; }
      internal Ped MovingTarget { get; set; }
      internal bool ObeyTrafficLaws { get; set; }
      internal float Speed { get; set; }
      internal Vector3 StaticTarget { get; set; }
      internal TypeOfTarget TargetType { get; set; }
      internal Vehicle Vehicle { get; set; }
   }
}