using Cyron43.GtaIV.Common;
using GTA;

namespace Autodrive
{
   public class SeatHandling : Script
   {
      private readonly Core _core;
      private readonly DriverHandling _driverHandling;

      public SeatHandling()
      {
      }

      public SeatHandling(DriverHandling driverHandling, Core core)
      {
         _core = core;
         _driverHandling = driverHandling;
      }

      internal void RemoveDriverAndWarpPlayerBackToDriversSeat()
      {
         if(CommonFunctions.PlayerIsStittingOnDriversSeat(Player.Character, _driverHandling.CurrentVehicle))
            return;
         _driverHandling.Driver = _driverHandling.CurrentVehicle.GetPedOnSeat(VehicleSeat.Driver);
         _driverHandling.DriverStopVehicleAndLeaveOrHover();
      }

      internal void WarpPlayerBackToDriverSeatOnceFree()
      {
         if(!CommonFunctions.VehicleExists(_driverHandling.CurrentVehicle)
            || !CommonFunctions.PedExists(Player.Character))
            return;
         while(!_driverHandling.CurrentVehicle.isSeatFree(VehicleSeat.Driver))
            Wait(Interval);
         Player.Character.WarpIntoVehicle(_driverHandling.CurrentVehicle, VehicleSeat.Driver);
      }

      internal bool WarpPlayerOntoFreeSeatAndGetDriver()
      {
         if(!SeatsAreRedistributable())
            return false;
         RedistributePassengerSeats();
         try
         {
            _core.ReadConfiguration();
            CreateDriver();
         }
         catch(NonExistingObjectException)
         {
            CommonFunctions
               .DisplayText("~r~Driver could not be created! Please check the model name in the config.~w~", 5000);
            WarpPlayerBackToDriverSeatOnceFree();
            _driverHandling.DeactivateAutodriveFlags();
            return false;
         }
         _driverHandling.ForceHelmetIfOnBikeFor(_driverHandling.Driver);
         if(CommonFunctions.PedExists(_driverHandling.Driver))
            return true;
         Player.Character.WarpIntoVehicle(_driverHandling.CurrentVehicle, VehicleSeat.Driver);
         Game.DisplayText("Failed to create driver!", 5000);
         return false;
      }

      private void CreateDriver()
      {
         if(_core.Configuration.AllowRandomDriverModel)
            _driverHandling.Driver = _driverHandling.CurrentVehicle
               .CreatePedOnSeat(VehicleSeat.Driver);
         else
            _driverHandling.Driver = _driverHandling.CurrentVehicle
               .CreatePedOnSeat(VehicleSeat.Driver, _core.Configuration.DriverModel);
      }

      private void RedistributePassengerSeats()
      {
         if(!_driverHandling.CurrentVehicle.isSeatFree(VehicleSeat.RightFront))
         {
            var passenger = _driverHandling.CurrentVehicle.GetPedOnSeat(VehicleSeat.RightFront);
            var freePassengerSeat = _driverHandling.CurrentVehicle.GetFreePassengerSeat();
            passenger.WarpIntoVehicle(_driverHandling.CurrentVehicle, freePassengerSeat);
         }
         Player.Character.WarpIntoVehicle(_driverHandling.CurrentVehicle, VehicleSeat.RightFront);
      }

      private bool SeatsAreRedistributable()
      {
         return _driverHandling.CurrentVehicle.GetFreePassengerSeat() != VehicleSeat.None;
      }
   }
}