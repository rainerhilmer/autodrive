﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTA;

namespace Autodrive
{
   /// <summary>
   ///    Implementierung eines eigenen Algorithmus für die Landung eines Hubschraubers in GTA IV.
   /// </summary>
   internal class HelicopterHandling
   {
      private const float INTERVAL_DISTANCE = 0.1f;

      /// <summary>
      ///    3. Berechne die Positionen entlang der Linie zum Landepunkt.
      /// </summary>
      /// <param name="initialApproachPosition"></param>
      /// <param name="landingPosition"></param>
      /// <returns>Liste mit Vektoren entlang der Landelinie.</returns>
      internal List<Vector3> PositionsDownToLanding(Vector3 initialApproachPosition, Vector3 landingPosition)
      {
         var intervals = CalculateAmountOfIntervals(CalculateGreatestDistance(initialApproachPosition, landingPosition));
         var xIntervalDistance = Math.Abs(initialApproachPosition.X - landingPosition.X) / intervals;
         var yIntervalDistance = Math.Abs(initialApproachPosition.Y - landingPosition.Y) / intervals;
         var zIntervalDistance = Math.Abs(initialApproachPosition.Z - landingPosition.Z) / intervals;

         var xArray = new float[intervals];
         var yArray = new float[intervals];
         var zArray = new float[intervals];

         float currentValue;

         if(initialApproachPosition.X > landingPosition.X)
         {
            currentValue = initialApproachPosition.X;
            for(var i = 0; i < intervals; i++)
            {
               xArray[i] = currentValue -= xIntervalDistance;
            }
         }

         if(initialApproachPosition.X < landingPosition.X)
         {
            currentValue = initialApproachPosition.X;
            for(var i = 0; i < intervals; i++)
            {
               xArray[i] = currentValue += xIntervalDistance;
            }
         }

         if(initialApproachPosition.Y > landingPosition.Y)
         {
            currentValue = initialApproachPosition.Y;
            for(var i = 0; i < intervals; i++)
            {
               yArray[i] = currentValue -= yIntervalDistance;
            }
         }

         if(initialApproachPosition.Y < landingPosition.Y)
         {
            currentValue = initialApproachPosition.Y;
            for(var i = 0; i < intervals; i++)
            {
               yArray[i] = currentValue += yIntervalDistance;
            }
         }

         if(initialApproachPosition.Z > landingPosition.Z)
         {
            currentValue = initialApproachPosition.Z;
            for(var i = 0; i < intervals; i++)
            {
               zArray[i] = currentValue -= zIntervalDistance;
            }
         }

         if(initialApproachPosition.Z < landingPosition.Z)
         {
            currentValue = initialApproachPosition.Z;
            for(var i = 0; i < intervals; i++)
            {
               zArray[i] = currentValue += zIntervalDistance;
            }
         }

         var vectorList = new List<Vector3>();

         for(var i = 0; i < intervals; i++)
         {
            vectorList.Add(new Vector3(xArray[i], yArray[i], zArray[i]));
         }
         return vectorList;
      }

      /// <summary>
      ///    2. Damit nicht auffällt, dass der Heli von einer Position zur
      ///    nächsten teleportiert, darf der Abstand einen bestimmten Wert
      ///    nicht übersteigen, damit das Auge getäuscht wird. Hier für den
      ///    Test nehme ich 0.5 Einheiten. Grundlage ist der größte Abstand.
      ///    Bei den beiden anderen Distanzen fällt der Interval-Abstand
      ///    entsprechend kleiner aus.
      /// </summary>
      private static int CalculateAmountOfIntervals(float greatestDistance)
      {
         return (int) (greatestDistance / INTERVAL_DISTANCE);
      }

      /// <summary>
      ///    1. Herausbekommen, welcher Abstand am größten ist.
      /// </summary>
      private static float CalculateGreatestDistance(Vector3 currentPosition, Vector3 landingPosition)
      {
         var xLength = Math.Abs(currentPosition.X - landingPosition.X);
         var yLength = Math.Abs(currentPosition.Y - landingPosition.Y);
         var zLength = Math.Abs(currentPosition.Z - landingPosition.Z);
         var list = new List<float> {xLength, yLength, zLength};
         return list.Max();
      }
   }
}