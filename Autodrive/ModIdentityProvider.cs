﻿using System.Reflection;
using Cyron43.GtaIV.Common;

namespace Autodrive
{
   public static class ModIdentityProvider
   {
      public static IModIdentity Identity
      {
         get
         {
            return new ModIdentity
                   {
                      AssemblyName = Assembly.GetExecutingAssembly().GetName().Name,
                      FullPath = CommonFunctions.FileRepositoryPath + "AutodriveConfig.xml",
                      Version = Assembly.GetExecutingAssembly().GetName().Version.Major
                   };
         }
      }
   }
}