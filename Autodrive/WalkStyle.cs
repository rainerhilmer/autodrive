﻿namespace Autodrive
{
   internal enum WalkStyle
   {
      Stop = 0,
      Walk,
      Run
   }
}
