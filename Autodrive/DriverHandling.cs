﻿using System;
using System.Collections.Generic;
using Autodrive.Strategies;
using Cyron43.GtaIV.Common;
using GTA;

namespace Autodrive
{
   public class DriverHandling : Script
   {
      private const float HIGH_SPEED = 70.0f;
      private const float INSANE_SPEED = 180.0f;
      private const float LOW_SPEED = 30.0f;
      private const float LOWMED_SPEED = 37.28227f; // = 60 Km/h
      private const float MEDIUM_SPEED = 45.0f;
      private const float PATROL_SPEED = 15.0f;
      private readonly Core _core;
      private readonly DrivingData _drivingData;
      private readonly SeatHandling _seatHandling;
      private bool _hasHelmetBecauseOfThisMod;
      private bool _isHovering;
      private Ped _movingTarget;
      private bool _playerIsDriver;
      private SpeedState _speedState;
      private bool _staticNavigationRequested;

      // ReSharper disable once UnusedMember.Global
      public DriverHandling()
      {
      }

      public DriverHandling(Core core)
      {
         _seatHandling = new SeatHandling(this, core);
         _core = core;
         Interval = 250;
         _drivingData = new DrivingData();
         _speedState = SpeedState.Low;
      }

      internal bool AutodriveRequested { get; set; }
      internal Vehicle CurrentVehicle { get; private set; }
      internal Ped Driver { get; set; }

      internal void ChangeAltitude(short altitudeChangeValue)
      {
         if(!CommonFunctions.VehicleExists(_drivingData.Vehicle))
            return;
         if(!_drivingData.Vehicle.Model.isHelicopter || _drivingData.TargetType == TypeOfTarget.None)
            return;
         _drivingData.DesiredAltitude = NewAltitudeInsideBoundaries(altitudeChangeValue);
         Game.DisplayText("Altitude set to " + _drivingData.DesiredAltitude + "ft.", 2000);
         UseNavigationStrategy();
      }

      internal void ChangeSpeed(bool increase)
      {
         if(!_drivingData.DrivingIsActive)
            return;
         CurrentVehicle.FreezePosition = false;
         UpdateSpeedStateWithCurrentSpeed(increase);
         if(increase)
            _speedState++;
         else
            _speedState--;
         KeepSpeedStateInsideBoundaries();
         SetDrivingDataAccordingToRequest();
      }

      internal void ChangeSpeedFine(bool increase)
      {
         if(!_drivingData.DrivingIsActive
            || CurrentVehicle.Model.isHelicopter
            || CurrentVehicle.Model.isBoat)
            return;
         if(increase)
         {
            if(_speedState == SpeedState.LowMed)
               _drivingData.Speed = 40.0f;
            else
               _drivingData.Speed += 5.0f;
         }
         else
         {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if(_speedState == SpeedState.LowMed && _drivingData.Speed == LOWMED_SPEED)
               _drivingData.Speed = 35.0f;
            else
               _drivingData.Speed -= 5.0f;
         }

         if(_drivingData.Speed < 0.0f)
            _drivingData.Speed = 0.0f;
         if(_drivingData.Speed > 180.0f)
            _drivingData.Speed = 180.0f;

         //UpdateSpeedStateWithCurrentSpeed();

         CreateDrivingDataMessage("Changed speed to " + _drivingData.Speed + "Mph.");

         if(_drivingData.Speed >= MEDIUM_SPEED)
            _drivingData.ObeyTrafficLaws = _core.Configuration.ObeyTrafficLawsOnHighSpeed;
         if(_drivingData.Speed <= LOWMED_SPEED)
            _drivingData.ObeyTrafficLaws = _core.Configuration.ObeyTrafficLawsOnLowSpeed;

         UseNavigationStrategy();
      }

      internal void DeactivateAutodriveFlags()
      {
         AutodriveRequested = false;
         _drivingData.DrivingIsActive = false;
         _drivingData.TargetType = TypeOfTarget.None;
         _drivingData.MovingTarget = null;
         _drivingData.StaticTarget = default(Vector3);
         _core.Tick -= HandleDriver;
      }

      internal void DriverStopVehicleAndLeaveOrHover()
      {
         if(!CommonFunctions.PedExists(Driver))
            return;
         if(CurrentVehicle.Model.isHelicopter && !CurrentVehicle.isOnAllWheels)
            Hover();
         else
         {
            Driver.Task.CruiseWithVehicle(
               CurrentVehicle, 0.0f,
               ObeyTrafficLaws: false);
            WaitForVehicleToStop();
            ReleaseDriver();
         }
      }

      internal void ForceHelmetIfOnBikeFor(Ped driver)
      {
         if(!CurrentVehicle.Model.isBike)
            return;
         driver.ForceHelmet(true);
         if(ReferenceEquals(driver, Player.Character))
            _hasHelmetBecauseOfThisMod = true;
      }

      internal void Honk()
      {
         if(!_drivingData.DrivingIsActive)
            return;
         CurrentVehicle.SoundHorn(1500);
      }

      internal void ToggleMovingTargetNavigation()
      {
         if(AutodriveRequested)
         {
            _staticNavigationRequested = false;
            Trigger();
         }
         else
            BailOut();
      }

      internal void ToggleSiren()
      {
         if(!_drivingData.DrivingIsActive)
            return;
         if(CurrentVehicle.Exists() && !CurrentVehicle.isOnFire)
            CurrentVehicle.SirenActive = !CurrentVehicle.SirenActive;
      }

      /// <summary>
      /// Called by the KeyDown event.
      /// </summary>
      internal void ToggleStaticTargetNavigation()
      {
         if(AutodriveRequested)
         {
            _staticNavigationRequested = true;
            Trigger();
         }
         else
            BailOut();
      }

      /// <summary>
      /// Is called by the TriggerLanding key.
      /// </summary>
      internal void TriggerLanding()
      {
         if(CurrentVehicle == null
            || !CurrentVehicle.Model.isHelicopter
            || CurrentVehicle.Model.isHelicopter && CurrentVehicle.isOnAllWheels)
            return;
         LandHelicopter();
      }

      private static List<Ped> SortFoundPedsByDistance(IList<Ped> pedsAround, Vector3 target)
      {
         var sorted = new List<Ped>();
         var currentPed = pedsAround[0];
         foreach(var ped in pedsAround)
         {
            if(ped.Position.DistanceTo2D(target) > currentPed.Position.DistanceTo2D(target))
               continue;
            currentPed = ped;
            sorted.Add(ped);
         }
         return sorted;
      }

      /// <summary>
      /// This method is called by the KeyDown event and stops the driver.
      /// </summary>
      private void BailOut()
      {
         if(!_playerIsDriver && _drivingData.TargetType != TypeOfTarget.None)
         {
            if(CommonFunctions.PedExists(Driver))
            {
               Driver.Task.ClearAllImmediately();
               Driver.Delete();
            }
            Player.Character.WarpIntoVehicle(CurrentVehicle, VehicleSeat.Driver);
         }
         if(_playerIsDriver)
         {
            Player.Character.Task.AlwaysKeepTask = false;
            Player.Character.Task.ClearAll();
            _playerIsDriver = false;
         }
         if(!_playerIsDriver)
            DriverStopVehicleAndLeaveOrHover();
         else
            PlayerStopVehicle();
         DeactivateAutodriveFlags();
         if(CurrentVehicle != null)
            CurrentVehicle.FreezePosition = false;
         UnforceHelmet();
         Game.DisplayText("Driving task has been released.", 5000);
      }

      private void BailoutIfTargetLost()
      {
         if(CommonFunctions.PedExists(_drivingData.MovingTarget))
            return;
         CommonFunctions.DisplayText("Target lost.", 3000);
         BailOut();
      }

      private void CheckIfDriverIsStillInVehicle()
      {
         if(!CommonFunctions.PedExists(_drivingData.Driver) || !CommonFunctions.VehicleExists(_drivingData.Vehicle))
            return;
         if(_drivingData.Driver.isSittingInVehicle(_drivingData.Vehicle))
            return;
         BailOut();
      }

      /// <summary>
      /// Creates an information message from the given driving data
      /// if not given with the message parameter.
      /// </summary>
      /// <param name="message">Use String.Empty or "" for an autonomous creation.</param>
      private void CreateDrivingDataMessage(string message)
      {
         if(!String.IsNullOrEmpty(message))
         {
            CommonFunctions.DisplayText(message, 3000);
            return;
         }
         const string part1 = "Attempting to ";
         const string part2 = " speed now (";
         const string part3 = " Mph).";
         var cruiseOrDriveOrChase = string.Empty;
         if(_drivingData.TargetType == TypeOfTarget.None)
            cruiseOrDriveOrChase = "cruise";
         if(_drivingData.TargetType != TypeOfTarget.None && !_drivingData.Vehicle.Model.isHelicopter)
            cruiseOrDriveOrChase = "drive";
         if(_drivingData.TargetType != TypeOfTarget.None && _drivingData.Vehicle.Model.isHelicopter)
            cruiseOrDriveOrChase = "fly";
         if(_drivingData.TargetType == TypeOfTarget.Moving)
            cruiseOrDriveOrChase = "chase";
         var speedState = _speedState.ToString().ToLower();
         if(!CurrentVehicle.Model.isHelicopter)
            _drivingData.Message = part1 + cruiseOrDriveOrChase + " with " + speedState + part2 + _drivingData.Speed +
                                   part3;
         else if(speedState == "insane")
            speedState = "maximum";
         _drivingData.Message = part1 + cruiseOrDriveOrChase + " with " + speedState + " speed now.";
         CommonFunctions.DisplayText(_drivingData.Message, 3000);
      }

      private void DecideForStaticOrMovingTargetNavigation()
      {
         if(!GetMovingTarget() || _staticNavigationRequested)
         {
            _drivingData.StaticTarget = GetWaypointDependingOnVehicleType();
            InitializeTargetNavigation(TypeOfTarget.Static);
         }
         else
         {
            _drivingData.MovingTarget = _movingTarget;
            InitializeTargetNavigation(TypeOfTarget.Moving);
         }
      }

      /// <summary>
      /// Gets any ped under the waypoint marker as long as it's no cop.
      /// </summary>
      /// <returns><c>false</c> if no ped (other than cop) could be found.</returns>
      private bool GetMovingTarget()
      {
         var waypointMarker = Game.GetWaypoint();
         if(waypointMarker == null)
            return false;
         var pedsAround = CommonFunctions.GetPedsSafe(waypointMarker.Position, 50.0f);
         if(pedsAround == null || pedsAround.Length == 0)
            return false;
         Ped closestPed = null;
         var sortedPeds = SortFoundPedsByDistance(pedsAround, waypointMarker.Position);
         if(sortedPeds.Count == 0)
            return false;
         foreach(var ped in sortedPeds)
         {
            if(CommonFunctions.PedExists(ped) && ped.RelationshipGroup != RelationshipGroup.Cop)
               closestPed = ped;
         }
         if(closestPed == null || !CommonFunctions.PedExists(closestPed))
            return false;
         _movingTarget = closestPed;
         return true;
      }

      private Vector3 GetWaypointDependingOnVehicleType()
      {
         var waypoint = Game.GetWaypoint().Position;
         if(!CurrentVehicle.Model.isCar && !CurrentVehicle.Model.isBike)
            return waypoint;
         if(_core.Configuration.StopOnStreet)
            return World.GetNextPositionOnStreet(waypoint);
         return waypoint;
      }

      private void HandleDriver(object sender, EventArgs eventArgs)
      {
         if(!AutodriveRequested)
            return;
         CheckIfDriverIsStillInVehicle();
         UpdateTarget();
         StopIfTargetReached();
         StopDriverIfPlayerLeftVehicle();
      }

      private bool HasFreePassengerSeat()
      {
         return !Player.Character.CurrentVehicle.GetFreePassengerSeat().Equals(VehicleSeat.None);
      }

      private void Hover()
      {
         if(_isHovering)
            return;
         _isHovering = true;
         _drivingData.DesiredAltitude = (short)CurrentVehicle.Position.Z;
         Wait(1000);
         _speedState = SpeedState.Stop;
         KeepSpeedStateInsideBoundaries();
         SetDrivingDataAccordingToRequest();
         CurrentVehicle.FreezePosition = true;
      }

      private void InitializeTargetNavigation(TypeOfTarget targetType)
      {
         if(CurrentVehicle == null
            || !CurrentVehicle.Exists()
            || CurrentVehicle.Model.Equals(default(Model)))
            return;
         CurrentVehicle.FreezePosition = false;
         var driver = CurrentVehicle.GetPedOnSeat(VehicleSeat.Driver);
         if(CommonFunctions.PedExists(driver))
            driver.Task.ClearAll();
         if(CurrentVehicle.Model.isHelicopter)
         {
            _drivingData.Vehicle = CurrentVehicle;
            SetInitialAltitude();
            _speedState = SpeedState.High;
         }
         _drivingData.TargetType = targetType;
         SetDrivingDataAccordingToRequest();
      }

      private void KeepSpeedStateInsideBoundaries()
      {
         if((int)_speedState >= Enum.GetValues(typeof(SpeedState)).Length)
            _speedState = SpeedState.Insane;
         if((int)_speedState < 1)
            _speedState = SpeedState.Stop;
      }

      private void LandHelicopter()
      {
         if(CurrentVehicle == null)
            return;
         CurrentVehicle.FreezePosition = false;
         if(_drivingData.StaticTarget == default(Vector3))
         {
            Game.DisplayText("Target lost.", 3000);
            return;
         }
         if(CurrentVehicle.Position.DistanceTo2D(_drivingData.StaticTarget) > 20)
         {
            CommonFunctions.DisplayText("Distance to landing spot too big."
                                        + " Please fly nearer to the landing spot and try again.", 3000);
            return;
         }
         Tick -= HandleDriver;
         CommonFunctions.DisplayText("Landing Helicopter. Pease wait...", 3000);
         Wait(500); // Wird benötigt, weil diese Methode sonst abbricht und den driving task released!
         var vectorList = new HelicopterHandling()
            .PositionsDownToLanding(CurrentVehicle.Position, _drivingData.StaticTarget);
         foreach(var vector in vectorList)
         {
            CurrentVehicle.Position = vector;
            if(CurrentVehicle.isOnAllWheels)
            {
               break;
            }
            Wait(10);
         }
         CurrentVehicle.PlaceOnGroundProperly();
         if(_drivingData.Driver != Player.Character)
            CurrentVehicle.FreezePosition = true;
         // The driver leaves the vehicle through the event driven StopIfTargetReached method.
      }

      private void MakeDriverInvincible(bool isTrue)
      {
         Driver.Invincible = isTrue;
         Driver.CanBeDraggedOutOfVehicle = isTrue;
         Driver.WillFlyThroughWindscreen = isTrue;
         Driver.CanBeKnockedOffBike = isTrue;
      }

      private short NewAltitudeInsideBoundaries(short altitudeChangeValue)
      {
         if((_drivingData.Vehicle.isOnAllWheels && altitudeChangeValue < 0)
            || (_drivingData.DesiredAltitude == 0 && altitudeChangeValue < 0))
            return _drivingData.DesiredAltitude;
         if(_drivingData.DesiredAltitude + altitudeChangeValue <= 0)
            return 0;
         return CommonFunctions.RoundToFactor((short) (_drivingData.DesiredAltitude + altitudeChangeValue), 20);
      }
      
      private void ObserveIfPlayerLeftVehicleForHelmetRemoval(object sender, EventArgs eventArgs)
      {
         UnforceHelmet();
         if(!_hasHelmetBecauseOfThisMod)
            _core.Tick -= ObserveIfPlayerLeftVehicleForHelmetRemoval;
      }

      private void PlayerStopVehicle()
      {
         if(CurrentVehicle.Model.isHelicopter && !CurrentVehicle.isOnAllWheels)
            Hover();
         else
         {
            Player.Character.Task.CruiseWithVehicle(
               CurrentVehicle, 0.0f,
               ObeyTrafficLaws: false);
            WaitForVehicleToStop();
            ReleasePlayer();
         }
      }

      private void PrepareCruisingWithDriver()
      {
         if(!_seatHandling.WarpPlayerOntoFreeSeatAndGetDriver())
            return;
         _drivingData.TargetType = TypeOfTarget.None;
         PrepareDriver();
         ForceHelmetIfOnBikeFor(Driver);
         SetDrivingDataAccordingToRequest();
      }

      private void PrepareCruisingWithPlayer()
      {
         _drivingData.TargetType = TypeOfTarget.None;
         Player.Character.Task.AlwaysKeepTask = true;
         ForceHelmetIfOnBikeFor(Player.Character);
         Game.DisplayText("Player has driving task now.", 5000);
         SetDrivingDataAccordingToRequest();
      }

      private void PrepareDriver()
      {
         if(!CommonFunctions.PedExists(Driver))
            return;
         Driver.BlockPermanentEvents = true;
         Driver.ChangeRelationship(RelationshipGroup.Player, Relationship.Respect);
         Driver.isRequiredForMission = true;
         if(CommonFunctions.VehicleExists(CurrentVehicle))
            CurrentVehicle.MakeProofTo(true, true, true, true, true);
         MakeDriverInvincible(true);
         Driver.Task.AlwaysKeepTask = true;
      }

      private void PrepareTargetNavigationWithDriver()
      {
         if(!_seatHandling.WarpPlayerOntoFreeSeatAndGetDriver())
            return;
         _speedState = SpeedState.Low;
         PrepareDriver();
         DecideForStaticOrMovingTargetNavigation();
      }

      private void PrepareTargetNavigationWithPlayer()
      {
         _speedState = SpeedState.Low;
         Game.DisplayText("Player has driving task now.", 5000);
         DecideForStaticOrMovingTargetNavigation();
      }

      private void ReleaseDriver()
      {
         if(!CommonFunctions.PedExists(Driver))
            return;
         MakeDriverInvincible(false);
         Driver.Task.AlwaysKeepTask = false;
         Driver.BlockPermanentEvents = false;
         Driver.ChangeRelationship(RelationshipGroup.Player, Relationship.Neutral);
         Driver.Task.ClearAll();
         Driver.Task.AlwaysKeepTask = true;
         Driver.isRequiredForMission = false;
         Driver.LeaveGroup();
         Driver.LeaveVehicle();
         Driver.Task.WanderAround();
         Driver.NoLongerNeeded();
         if(Driver.PedType == PedType.Cop)
         {
            Wait(3000);
            Driver.Delete();
         }
         Game.LoadAllPathNodes = false;
         _drivingData.TargetType = TypeOfTarget.None;
         DeactivateAutodriveFlags();
         if(Player.Character.isSittingInVehicle(CurrentVehicle))
            _seatHandling.WarpPlayerBackToDriverSeatOnceFree();
         Game.DisplayText("Driving task has been released.", 5000);
         CurrentVehicle.FreezePosition = false;
         _speedState = SpeedState.Low;
         SwitchTickEventToHelmetObserver();
      }

      private void ReleasePlayer()
      {
         Player.Character.Task.AlwaysKeepTask = false;
         /* The following two lines had to be done this way because the
          * player character was not released when the vehicle was bumped
          * against another vehicle! */
         Player.Character.Task.ClearAllImmediately();
         Player.Character.WarpIntoVehicle(CurrentVehicle, VehicleSeat.Driver);
         ForceHelmetIfOnBikeFor(Player.Character);
         Game.LoadAllPathNodes = false;
         _drivingData.TargetType = TypeOfTarget.None;
         DeactivateAutodriveFlags();
         _playerIsDriver = false;
         Game.DisplayText("Player driving task has been released.", 5000);
         CurrentVehicle.FreezePosition = false;
         _speedState = SpeedState.Low;
         SwitchTickEventToHelmetObserver();
      }

      private void SetDrivingDataAccordingToRequest()
      {
         var driver = CurrentVehicle.GetPedOnSeat(VehicleSeat.Driver);
         if(driver == null)
            return;
         _drivingData.Driver = driver;
         _drivingData.Speed = SetInitialSpeed();
         _drivingData.Vehicle = CurrentVehicle;

         switch(_speedState)
         {
            case SpeedState.Stop:
               _drivingData.AllowToDriveRoadsWrongWay = false;
               _drivingData.ObeyTrafficLaws = false;
               _drivingData.Speed = 0.0f;
               CreateDrivingDataMessage("Stopping...");
               UseNavigationStrategy();
               break;

            case SpeedState.Patrol:
               _drivingData.AllowToDriveRoadsWrongWay = false;
               _drivingData.ObeyTrafficLaws = _core.Configuration.ObeyTrafficLawsOnLowSpeed;
               _drivingData.Speed = PATROL_SPEED;
               CreateDrivingDataMessage(string.Empty);
               UseNavigationStrategy();
               break;

            case SpeedState.Low:
               _drivingData.AllowToDriveRoadsWrongWay = false;
               _drivingData.ObeyTrafficLaws = _core.Configuration.ObeyTrafficLawsOnLowSpeed;
               _drivingData.Speed = LOW_SPEED;
               CreateDrivingDataMessage(string.Empty);
               UseNavigationStrategy();
               break;

            case SpeedState.LowMed:
               _drivingData.AllowToDriveRoadsWrongWay = false;
               _drivingData.ObeyTrafficLaws = _core.Configuration.ObeyTrafficLawsOnLowSpeed;
               _drivingData.Speed = LOWMED_SPEED;
               CreateDrivingDataMessage(string.Empty);
               UseNavigationStrategy();
               break;

            case SpeedState.Medium:
               _drivingData.AllowToDriveRoadsWrongWay = false;
               _drivingData.ObeyTrafficLaws = _core.Configuration.ObeyTrafficLawsOnHighSpeed;
               _drivingData.Speed = MEDIUM_SPEED;
               CreateDrivingDataMessage(string.Empty);
               UseNavigationStrategy();
               break;

            case SpeedState.High:
               _drivingData.AllowToDriveRoadsWrongWay = true;
               _drivingData.ObeyTrafficLaws = _core.Configuration.ObeyTrafficLawsOnHighSpeed;
               _drivingData.Speed = HIGH_SPEED;
               CreateDrivingDataMessage(string.Empty);
               UseNavigationStrategy();
               break;

            case SpeedState.Insane:
               _drivingData.AllowToDriveRoadsWrongWay = true;
               _drivingData.ObeyTrafficLaws = _core.Configuration.ObeyTrafficLawsOnHighSpeed;
               _drivingData.Speed = INSANE_SPEED;
               CreateDrivingDataMessage(string.Empty);
               UseNavigationStrategy();
               break;
         }
      }

      private void SetInitialAltitude()
      {
         if(CurrentVehicle.isOnAllWheels)
            _drivingData.DesiredAltitude =
               (short) Math.Round(unchecked((double) _drivingData.Vehicle.Position.Z + CommonFunctions.MIN_ALT_FIX));
         else
            _drivingData.DesiredAltitude = (short) Math.Round(unchecked(_drivingData.Vehicle.Position.Z));
      }

      private float SetInitialSpeed()
      {
         if(!CommonFunctions.VehicleExists(CurrentVehicle)
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            || CurrentVehicle.Speed == 0.0f)
            return LOW_SPEED;
         return CurrentVehicle.Speed;
      }

      private void SetSpeedStateNearestToCurrentSpeed()
      {
         // ReSharper disable CompareOfFloatsByEqualityOperator
         if(_drivingData.Speed == 0.0f)
            _speedState = SpeedState.Stop;
         if(_drivingData.Speed > 0.0f && _drivingData.Speed <= PATROL_SPEED)
            _speedState = SpeedState.Patrol;
         if(_drivingData.Speed > PATROL_SPEED && _drivingData.Speed <= LOW_SPEED)
            _speedState = SpeedState.Low;
         if(_drivingData.Speed > LOW_SPEED && _drivingData.Speed <= LOWMED_SPEED)
            _speedState = SpeedState.LowMed;
         if(_drivingData.Speed > LOWMED_SPEED && _drivingData.Speed <= MEDIUM_SPEED)
            _speedState = SpeedState.Medium;
         if(_drivingData.Speed > MEDIUM_SPEED && _drivingData.Speed <= HIGH_SPEED)
            _speedState = SpeedState.High;
         if(_drivingData.Speed > HIGH_SPEED)
            _speedState = SpeedState.Insane;
         // ReSharper restore CompareOfFloatsByEqualityOperator
      }

      private void StopDriverIfPlayerLeftVehicle()
      {
         if(Player.Character.isInVehicle())
            return;
         if(CurrentVehicle == null)
            return;
         DriverStopVehicleAndLeaveOrHover();
      }

      private void StopIfTargetReached()
      {
         if(_drivingData.TargetType == TypeOfTarget.None
            || !CommonFunctions.VehicleExists(CurrentVehicle)
            || _drivingData.TargetType == TypeOfTarget.Moving)
            return;
         if((CurrentVehicle.Position.DistanceTo2D(_drivingData.StaticTarget) > 10.0f))
            return;
         if(_playerIsDriver)
            PlayerStopVehicle();
         else
            DriverStopVehicleAndLeaveOrHover();
      }

      private void SwitchTickEventToHelmetObserver()
      {
         _core.Tick -= HandleDriver;
         _core.Tick += ObserveIfPlayerLeftVehicleForHelmetRemoval;
      }

      /// <summary>
      /// This method is called by the KeyDown event and starts the driver.
      /// </summary>
      private void Trigger()
      {
         if(!Player.Character.isSittingInVehicle() || !AutodriveRequested)
            return;
         CurrentVehicle = Player.Character.CurrentVehicle;
         if(!HasFreePassengerSeat() || _core.Configuration.AlwaysDriveYourself)
         {
            _playerIsDriver = true;
            _drivingData.Driver = Player.Character;
         }

         //Autodrive does not work with the player as driver on a boat for an unknown reason.
         if(CurrentVehicle.Model.isBoat && HasFreePassengerSeat())
            _playerIsDriver = false;
         if(CurrentVehicle.Model.isBoat && !HasFreePassengerSeat())
         {
            CommonFunctions.DisplayText("~y~Sorry but for an unknown reason "
                                        + "Autodrive won't work with the player as boat driver.~w~", 3000);
            DeactivateAutodriveFlags();
            return;
         }
         _core.ReadConfiguration();
         Game.LoadAllPathNodes = true;
         _speedState = SpeedState.Low;
         _seatHandling.RemoveDriverAndWarpPlayerBackToDriversSeat();
         _core.Tick -= ObserveIfPlayerLeftVehicleForHelmetRemoval;
         _core.Tick += HandleDriver;
         _isHovering = false;
         if(Game.GetWaypoint() != null)
         {
            if(_playerIsDriver)
               PrepareTargetNavigationWithPlayer();
            else
               PrepareTargetNavigationWithDriver();
         }
         if(_drivingData.TargetType != TypeOfTarget.None)
            return;
         if(CurrentVehicle.Model.isHelicopter)
         {
            CommonFunctions.DisplayText("Sorry but the driver can't fly helicopters without a destination.", 5000);
            DeactivateAutodriveFlags();
         }
         else if(_playerIsDriver)
            PrepareCruisingWithPlayer();
         else
            PrepareCruisingWithDriver();
      }

      private void UnforceHelmet()
      {
         if(!_hasHelmetBecauseOfThisMod || Player.Character.isInVehicle())
            return;
         Player.Character.ForceHelmet(false);
         _hasHelmetBecauseOfThisMod = false;
      }

      private void UpdateSpeedStateWithCurrentSpeed(bool increasing)
      {
         // ReSharper disable CompareOfFloatsByEqualityOperator
         if(_drivingData.Speed == 0.0f)
            _speedState = SpeedState.Stop;
         if(_drivingData.Speed > PATROL_SPEED && _drivingData.Speed < LOW_SPEED)
            _speedState = increasing ? SpeedState.Patrol : SpeedState.Low;

         if(_drivingData.Speed > LOW_SPEED && _drivingData.Speed < LOWMED_SPEED)
            _speedState = increasing ? SpeedState.Low : SpeedState.LowMed;

         if(_drivingData.Speed > LOWMED_SPEED && _drivingData.Speed < MEDIUM_SPEED)
            _speedState = increasing ? SpeedState.LowMed : SpeedState.Medium;

         if(_drivingData.Speed > MEDIUM_SPEED && _drivingData.Speed < HIGH_SPEED)
            _speedState = increasing ? SpeedState.Medium : SpeedState.High;

         if(_drivingData.Speed > HIGH_SPEED && _drivingData.Speed < INSANE_SPEED)
            _speedState = increasing ? SpeedState.High : SpeedState.Insane;
         // ReSharper restore CompareOfFloatsByEqualityOperator
      }

      private void UpdateTarget()
      {
         if(!AutodriveRequested)
         {
            BailOut();
            return;
         }
         if(_drivingData.TargetType == TypeOfTarget.Moving)
            UseNavigationStrategy();
         var waypoint = Game.GetWaypoint();
         if(waypoint == null)
            return;
         if(!WaypointChanged())
            return;
         _isHovering = false;
         _drivingData.StaticTarget = GetWaypointDependingOnVehicleType();
         SetSpeedStateNearestToCurrentSpeed();
         DecideForStaticOrMovingTargetNavigation();
      }

      private void UseNavigationStrategy()
      {
         if(CurrentVehicle.Model.isHelicopter)
         {
            if(_drivingData.TargetType == TypeOfTarget.Static)
               new FlyHelicopterToStaticWaypointStrategy().DriveWith(_drivingData);
            if(_drivingData.TargetType != TypeOfTarget.Moving)
               return;
            BailoutIfTargetLost();
            if(!AutodriveRequested)
               return;
            new FollowMovingTargetInHelicopterStrategy().DriveWith(_drivingData);
         }
         else
         {
            if(_drivingData.TargetType == TypeOfTarget.Static)
            {
               new DriveToStaticWaypointStrategy().DriveWith(_drivingData);
            }
            if(_drivingData.TargetType == TypeOfTarget.None)
               new CruiseOnGroundStrategy().DriveWith(_drivingData);
            if(_drivingData.TargetType != TypeOfTarget.Moving)
               return;
            BailoutIfTargetLost();
            if(!AutodriveRequested)
               return;
            new FollowMovingTargetOnGroundStrategy().DriveWith(_drivingData);
         }
      }

      private void WaitForVehicleToStop()
      {
         while(CurrentVehicle.Speed > 1)
            Wait(Interval);
      }

      private bool WaypointChanged()
      {
         var currentWaypoint = GetWaypointDependingOnVehicleType();
         // ReSharper disable CompareOfFloatsByEqualityOperator
         return _drivingData.StaticTarget.X != currentWaypoint.X
                || _drivingData.StaticTarget.Y != currentWaypoint.Y
                || _drivingData.StaticTarget.Z != currentWaypoint.Z;
         // ReSharper restore CompareOfFloatsByEqualityOperator
      }
   }
}