﻿using GTA;
using GTA.Native;
using GTA.value;

namespace Autodrive
{
   public static class Extensions
   {
      public static Vector3 CorrectToGround(this Vector3 position)
      {
         var elevatedPosition = new Vector3(position.X, position.Y, position.Z + 300);
         return elevatedPosition.ToGround();
      }
      
      /// <summary>
      ///    Native function call
      /// </summary>
      /// <param name="bla">Extention of Tasks.</param>
      /// <param name="waypoint">The waypoint.</param>
      /// <param name="moveType">2 for walking, 4 for running</param>
      public static void MyGoto(this Tasks bla, Ped ped, Vector3 waypoint, byte moveType)
      {
         //TASK_GO_STRAIGHT_TO_COORD
         /* Wenn TASK_GO_TO_COORD_ANY_MEANS benutzt wird, ist Luis zuerst
          * unentschlossen und steigt dann in ein Fahrzeug, um dann damit
          * zum Waypoint zu fahren.
          * TASK_FOLLOW_NAV_MESH_TO_COORD_NO_STOP funzt nicht.*/
         Function.Call("TASK_CHAR_SLIDE_TO_COORD", ped, waypoint.X, waypoint.Y, waypoint.Z, 0, 4);
      }
   }
}