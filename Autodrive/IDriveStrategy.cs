﻿namespace Autodrive
{
   internal interface IDriveStrategy
   {
      void DriveWith(DrivingData drivingData);
   }
}