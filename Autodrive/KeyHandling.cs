﻿using System.Linq;
using System.Windows.Forms;
using Cyron43.GtaIV.Common;
using GTA;
using KeyEventArgs = GTA.KeyEventArgs;

namespace Autodrive
{
   public class KeyHandling : Script
   {
      private readonly Core _core;
      private readonly DriverHandling _driverHandling;
      private readonly WalkHandling _walkHandling;
      //private readonly TestHelpers _testHelpers;

      // ReSharper disable once UnusedMember.Global
      public KeyHandling()
      {
      }

      public KeyHandling(Core core)
      {
         _core = core;
         _driverHandling = new DriverHandling(core);
         _walkHandling = new WalkHandling(core);
         //_testHelpers = new TestHelpers();
      }

      internal void HandleKey(KeyEventArgs e)
      {
         AtToggleStaticTargetNavigation(e);
         AtToggleMovingTargetNavigation(e);
         AtActionKeyWhileOnAutopilot(e);
         AtSpeedUp(e);
         AtSpeedUpFine(e);
         AtSlowDown(e);
         AtSlowDownFine(e);
         AtHonk(e);
         AtToggleSiren(e);
         AtDecreaseAltitude(e);
         AtIncreaseAltitude(e);
         AtTriggerLanding(e);
         //AtToggleTest(e);
      }

      private void AtActionKeyWhileOnAutopilot(KeyEventArgs e)
      {
         var disabledKeys = new[] {Keys.F, Keys.W, Keys.S, Keys.A, Keys.D};
         var disabledKeysForChopper = new[] {Keys.F, Keys.A, Keys.D};
         if(CommonFunctions.VehicleExists(_driverHandling.CurrentVehicle))
         {
            if(_driverHandling.CurrentVehicle.Model.isHelicopter)
            {
               if(_driverHandling.AutodriveRequested
                  && disabledKeysForChopper.Any(key => e.Key == key))
                  CommonFunctions.DisplayText("~y~Helicopter is still on autopilot.~w~", 3000);
            }
            else
            {
               if(_driverHandling.AutodriveRequested
                  && disabledKeys.Any(key => e.Key == key))
                  CommonFunctions.DisplayText("~y~Vehicle is still on autopilot.~w~", 3000);
            }
         }
         if(_walkHandling.AutoWalkRequested
            && disabledKeys.Any(key => e.Key == key))
            CommonFunctions.DisplayText("~y~Player character is still on autowalk.~w~", 3000);
      }

      private void AtDecreaseAltitude(KeyEventArgs e)
      {
         var decrAltKey = new KeyContainer {Key = Keys.S};
         if(KeyHandlingCommons.KeyIs(decrAltKey, e)
            && HelicopterAutopilotKeyConditionsAreGiven())
            _driverHandling.ChangeAltitude(-20);
      }

      private void AtHonk(KeyEventArgs e)
      {
         var hornKey = new KeyContainer {Key = Keys.G};
         if(!KeyHandlingCommons.KeyIs(hornKey, e))
            return;
         _driverHandling.Honk();
      }

      /// <summary>
      /// Wird nur benötigt, um ein sich bewegendes Ziel für Testzwecke zu generieren.
      /// </summary>
      //private void AtToggleTest(KeyEventArgs keyEventArgs)
      //{
      //   var testKey = new KeyContainer { Key = Keys.T };
      //   if(!KeyIs(testKey, keyEventArgs))
      //      return;
      //   //_testHelpers.AnalyzeThingsUnderMarker();
      //   if(!_testHelpers.MovingTargetIsActive)
      //      _testHelpers.CreateMovingTarget();
      //   else
      //      _testHelpers.DisposeMovingTarget();
      //}
      
      private void AtIncreaseAltitude(KeyEventArgs e)
      {
         var incrAltKey = new KeyContainer {Key = Keys.W};
         if(KeyHandlingCommons.KeyIs(incrAltKey, e)
            && HelicopterAutopilotKeyConditionsAreGiven())
            _driverHandling.ChangeAltitude(20);
      }

      private void AtSlowDown(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "SlowDown"), e))
            return;
         if(_walkHandling.AutoWalkRequested)
            _walkHandling.SlowDown();
         else
            _driverHandling.ChangeSpeed(increase: false);
      }

      private void AtSlowDownFine(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "SlowDownFine"), e))
            return;
         _driverHandling.ChangeSpeedFine(increase: false);
      }

      private void AtSpeedUp(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "SpeedUp"), e))
            return;
         if(_walkHandling.AutoWalkRequested)
            _walkHandling.SpeedUp();
         else
            _driverHandling.ChangeSpeed(increase: true);
      }

      private void AtSpeedUpFine(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "SpeedUpFine"), e))
            return;
         _driverHandling.ChangeSpeedFine(increase: true);
      }

      private void AtToggleMovingTargetNavigation(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "ToggleMovingTargetNavigation"), e))
            return;
         _core.ReadConfiguration();
         _driverHandling.AutodriveRequested = !_driverHandling.AutodriveRequested;
         _driverHandling.ToggleMovingTargetNavigation();
      }

      private void AtToggleSiren(KeyEventArgs e)
      {
         var hornKey = new KeyContainer { Key = Keys.S };
         if(!KeyHandlingCommons.KeyIs(hornKey, e))
            return;
         _driverHandling.ToggleSiren();
      }

      private void AtToggleStaticTargetNavigation(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "ToggleStaticTargetNavigation"), e))
            return;
         _core.ReadConfiguration();
         if(!Player.Character.isInVehicle())
         {
            ReleaseVehicleAutopilotIfWalkHandlingIsRequested();
            _walkHandling.AutoWalkRequested = !_walkHandling.AutoWalkRequested;
            _walkHandling.ToggleWalking();
         }
         else if(Player.Character.isSittingInVehicle())
         {
            _driverHandling.AutodriveRequested = !_driverHandling.AutodriveRequested;
            _driverHandling.ToggleStaticTargetNavigation();
         }
         else
         {
            _driverHandling.AutodriveRequested = false;
            _walkHandling.AutoWalkRequested = false;
         }
      }

      private void AtTriggerLanding(KeyEventArgs e)
      {
         if(!KeyHandlingCommons.KeyIs(
            KeyHandlingCommons.GetKeyContainer(
               _core.Configuration.Keys, "TriggerLanding"), e))
            return;
         _driverHandling.TriggerLanding();
      }

      private bool HelicopterAutopilotKeyConditionsAreGiven()
      {
         return Player.Character.isSittingInVehicle()
                && Player.Character.CurrentVehicle.Model.isHelicopter
                && _driverHandling.AutodriveRequested;
      }

      private void ReleaseVehicleAutopilotIfWalkHandlingIsRequested()
      {
         if(!_driverHandling.AutodriveRequested)
            return;
         _driverHandling.AutodriveRequested = false;
         _driverHandling.ToggleStaticTargetNavigation();
      }
   }
}