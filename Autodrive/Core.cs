﻿using Cyron43.GtaIV.Common;
using GTA;

namespace Autodrive
{
   // ReSharper disable once ClassNeverInstantiated.Global
   public class Core : Script
   {
      private readonly KeyHandling _keyHandling;
      private ErrorType _typeOfError;

      public Core()
      {
         ReadConfiguration();
         if(_typeOfError != ErrorType.None)
            return;
         _keyHandling = new KeyHandling(this);
         KeyDown += OnKeyDown;
      }

      internal ConfigurationContainer Configuration { get; private set; }

      internal void ReadConfiguration()
      {
         Configuration = ConfigurationProvider
            .CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(ModIdentityProvider.Identity,
               out _typeOfError);
      }

      private void OnKeyDown(object sender, KeyEventArgs e)
      {
         _keyHandling.HandleKey(e);
      }
   }
}