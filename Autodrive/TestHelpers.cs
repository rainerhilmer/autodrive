﻿//using GTA;
////using DoNeX.Logging;

//namespace Autodrive
//{
//   public class TestHelpers : Script
//   {
//      private Blip _blip;
//      private Ped _driver;
//      private Vehicle _vehicle;
//      //private readonly Logger _log;

//      // ReSharper disable once EmptyConstructor
//      public TestHelpers()
//      {
//         //_log =
//         //   new TextLogger(@"I:\wip\GTA4-Mods\Autodrive\Autodrive\Autodrive\bin\x86\Release\Autodrive.log");
//      }

//      internal bool MovingTargetIsActive { get; set; }

//      internal void AnalyzeThingsUnderMarker()
//      {
//         //if(_log.Exists)
//         //   _log.Delete();
//         //_log.Create("Autodrive test session of " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
//         var marker = Game.GetWaypoint();
//         var pedsAround = CommonFunctions.GetPedsSafe(marker.Position, 50.0f);
//         foreach (var ped in pedsAround)
//         {
//            //_log.Write(ped.RelationshipGroup.ToString());
//            //_log.Write("Ped type" + ped.PedType);
//            //_log.Write("-----");
//         }
//         var vehiclesAround = World.GetVehicles(marker.Position, 50.0f);
//         foreach (var vehicle in vehiclesAround)
//         {
//            //_log.Write(vehicle.Name);
//         }
//         CommonFunctions.DisplayText("Analyze done.", 3000);
//      }

//      internal void CreateMovingTarget()
//      {
//         _vehicle = World.CreateVehicle(new Model("AIRTUG"), Player.Character.Position.Around(100.0f));
//         if (!CommonFunctions.VehicleExists(_vehicle))
//            return;
//         _vehicle.PlaceOnNextStreetProperly();
//         _vehicle.HazardLightsOn = true;
//         _driver = _vehicle.CreatePedOnSeat(VehicleSeat.Driver);
//         if (!CommonFunctions.PedExists(_driver))
//            return;
//         _blip = Blip.AddBlip(_driver);
//         _blip.Name = "Test";
//         _blip.Color = BlipColor.DarkTurquoise;
//         _driver.Task.AlwaysKeepTask = true;
//         //_driver.RelationshipGroup = RelationshipGroup.Criminal;
//         _driver.BecomeMissionCharacter();
//         //_driver.Enemy = true;
//         _driver.Task.CruiseWithVehicle(_vehicle, CommonFunctions.CorrectedSpeed(30.0f), ObeyTrafficLaws: false);
//         MovingTargetIsActive = true;
//      }

//      internal void DisposeMovingTarget()
//      {
//         if (CommonFunctions.PedExists(_driver))
//         {
//            _blip.Delete();
//            _driver.Delete();
//         }
//         if (CommonFunctions.VehicleExists(_vehicle))
//            _vehicle.Delete();
//         MovingTargetIsActive = false;
//      }
//   }
//}

