﻿using Cyron43.GtaIV.Common;

namespace Autodrive.Strategies
{
   internal class CruiseOnGroundStrategy : IDriveStrategy
   {
      public void DriveWith(DrivingData drivingData)
      {
         drivingData.Driver.Task.AlwaysKeepTask = true;
         drivingData.DrivingIsActive = true;
         drivingData.Driver.Task.CruiseWithVehicle(drivingData.Vehicle,
            CommonFunctions.CorrectedSpeed(drivingData.Speed),
            drivingData.ObeyTrafficLaws);
      }
   }
}