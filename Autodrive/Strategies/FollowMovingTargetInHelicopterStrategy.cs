﻿using Cyron43.GtaIV.Common;
using GTA.Native;

namespace Autodrive.Strategies
{
   internal class FollowMovingTargetInHelicopterStrategy : IDriveStrategy
   {
      public void DriveWith(DrivingData drivingData)
      {
         if(drivingData.DesiredAltitude < drivingData.MovingTarget.Position.Z)
            drivingData.DesiredAltitude = (short) (drivingData.MovingTarget.Position.Z + CommonFunctions.MIN_ALT_FIX);

         drivingData.DrivingIsActive = true;
         Function.Call("TASK_HELI_MISSION", drivingData.Driver, drivingData.Vehicle, 0, 0,
            drivingData.MovingTarget.Position.X, drivingData.MovingTarget.Position.Y,
            drivingData.MovingTarget.Position.Z, 4, CommonFunctions.CorrectedSpeed(drivingData.Speed), 10, -1, -1,
            drivingData.DesiredAltitude);
      }
   }
}