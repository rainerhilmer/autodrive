using Cyron43.GtaIV.Common;

namespace Autodrive.Strategies
{
   internal class FollowMovingTargetOnGroundStrategy : IDriveStrategy
   {
      public void DriveWith(DrivingData drivingData)
      {
         drivingData.Driver.Task.AlwaysKeepTask = true;
         drivingData.DrivingIsActive = true;
         drivingData.Driver.Task.DriveTo(drivingData.MovingTarget,
            CommonFunctions.CorrectedSpeed(drivingData.Speed),
            drivingData.ObeyTrafficLaws, drivingData.AllowToDriveRoadsWrongWay);
      }
   }
}