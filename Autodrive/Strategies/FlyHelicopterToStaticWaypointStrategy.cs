﻿using Cyron43.GtaIV.Common;
using GTA.Native;

namespace Autodrive.Strategies
{
   internal class FlyHelicopterToStaticWaypointStrategy : IDriveStrategy
   {
      public void DriveWith(DrivingData drivingData)
      {
         if(drivingData.DesiredAltitude < drivingData.StaticTarget.Z)
            drivingData.DesiredAltitude = (short) (drivingData.StaticTarget.Z + CommonFunctions.MIN_ALT_FIX);

         drivingData.DrivingIsActive = true;
         drivingData.Driver.Task.ClearAll();
         Function.Call("TASK_HELI_MISSION", drivingData.Driver, drivingData.Vehicle, 0, 0, drivingData.StaticTarget.X,
            drivingData.StaticTarget.Y, drivingData.StaticTarget.Z, 4, CommonFunctions.CorrectedSpeed(drivingData.Speed),
            10, -1, -1, drivingData.DesiredAltitude);
      }
   }
}