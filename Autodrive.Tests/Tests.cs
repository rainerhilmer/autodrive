﻿using System;
using System.Collections.Generic;
using Cyron43.GtaIV.Common.FakesForTests;
using FluentAssertions;
using NUnit.Framework;

namespace Autodrive.Tests
{
   [TestFixture]
   public class Tests
   {
      [Test]
      public void LandingAlgorithm_returns_correct_positions()
      {
         var sut = new FakeLandingAlgorithm();
         var startPosition = new Vector3(-10, 10, 25);
         var endPosition = new Vector3(0, 0, 0);
         var positions = sut.PositionsDownToLanding(startPosition, endPosition);
         foreach (var position in positions)
         {
            Console.WriteLine("X:{0}/Y:{1}/Z:{2}", position.X, position.Y, position.Z);
         }
      }

      [Test]
      public void Removes_10_percent_at_the_end_of_a_list()
      {
         var list = new List<int>();
         for (int i = 1; i < 68; i++)
         {
            list.Add(i);
         }
         var tenPercent = list.Count / 10;
         list.RemoveRange(list.Count - tenPercent, tenPercent);
         foreach (var item in list)
         {
            Console.WriteLine(item);
         }
         list.Count.Should().Be(61);
      }
   }
}