﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Cyron43.GtaIV.Common;
using FluentAssertions;
using NUnit.Framework;

namespace Autodrive.Tests
{
   [TestFixture]
   public class ConfigurationTests
   {
      [Test]
      public void Read_config_raw_display()
      {
         ErrorType typeOfError;
         var sut = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(ModIdentityProvider.Identity, out typeOfError);
         Console.WriteLine("AllowRandomDriverModel: " + sut.AllowRandomDriverModel);
         Console.WriteLine("AlwaysDriveYourself: " + sut.AlwaysDriveYourself);
         Console.WriteLine("DriverModel: " + sut.DriverModel);
         Console.WriteLine("ObeyTrafficLawsOnLowSpeed: " + sut.ObeyTrafficLawsOnLowSpeed);
      }

      [Test]
      public void Reads_config()
      {
         ErrorType typeOfError;
         var sut = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity)
            .GetConfigurationFor<ConfigurationContainer>(
               ModIdentityProvider.Identity,
               out typeOfError,
               supressInGameMessage: true);

         sut.AllowRandomDriverModel.Should().BeTrue("AllowRandomDriverModel");
         sut.AlwaysDriveYourself.Should().BeTrue("AlwaysDriveYourself");
         sut.DriverModel.Should().Be("M_Y_ROMANCAB");
         sut.ObeyTrafficLawsOnHighSpeed.Should().BeFalse();
         sut.ObeyTrafficLawsOnLowSpeed.Should().BeTrue("ObeyTrafficLawsOnLowSpeed");
         
         #region key container tests
         var slowDownKey = sut.Keys.Find(x => x.Name == "SlowDown");
         slowDownKey.Should().NotBeNull("slowDownKey");
         slowDownKey.Alt.Should().BeFalse("KeyContainer.Alt");
         slowDownKey.Ctrl.Should().BeFalse("KeyContainer.Ctrl");
         slowDownKey.Key.Should().Be(Keys.Left, "KeyContainer.Key");
         slowDownKey.Name.Should().Be("SlowDown", "KeyContainer.Name");
         slowDownKey.Shift.Should().BeFalse("KeyContainer.Shift");

         var slowDownFineKey = sut.Keys.Find(x => x.Name == "SlowDownFine");
         slowDownFineKey.Should().NotBeNull("slowDownFineKey");
         slowDownFineKey.Alt.Should().BeFalse("KeyContainer.Alt");
         slowDownFineKey.Ctrl.Should().BeTrue("KeyContainer.Ctrl");
         slowDownFineKey.Key.Should().Be(Keys.Left, "KeyContainer.Key");
         slowDownFineKey.Name.Should().Be("SlowDownFine", "KeyContainer.Name");
         slowDownFineKey.Shift.Should().BeFalse("KeyContainer.Shift");

         var speedUpKey = sut.Keys.Find(x => x.Name == "SpeedUp");
         speedUpKey.Should().NotBeNull("speedUpKey");
         speedUpKey.Alt.Should().BeFalse("KeyContainer.Alt");
         speedUpKey.Ctrl.Should().BeFalse("KeyContainer.Ctrl");
         speedUpKey.Key.Should().Be(Keys.Right, "KeyContainer.Key");
         speedUpKey.Name.Should().Be("SpeedUp", "KeyContainer.Name");
         speedUpKey.Shift.Should().BeFalse("KeyContainer.Shift");

         var speedUpFineKey = sut.Keys.Find(x => x.Name == "SpeedUpFine");
         speedUpFineKey.Should().NotBeNull("speedUpFineKey");
         speedUpFineKey.Alt.Should().BeFalse("KeyContainer.Alt");
         speedUpFineKey.Ctrl.Should().BeTrue("KeyContainer.Ctrl");
         speedUpFineKey.Key.Should().Be(Keys.Right, "KeyContainer.Key");
         speedUpFineKey.Name.Should().Be("SpeedUpFine", "KeyContainer.Name");
         speedUpFineKey.Shift.Should().BeFalse("KeyContainer.Shift");

         var toggleStaticTargetNavigationKey = sut.Keys.Find(x => x.Name == "ToggleStaticTargetNavigation");
         toggleStaticTargetNavigationKey.Should().NotBeNull("toggleStaticTargetNavigationKey");
         toggleStaticTargetNavigationKey.Alt.Should().BeFalse("KeyContainer.Alt");
         toggleStaticTargetNavigationKey.Ctrl.Should().BeFalse("KeyContainer.Ctrl");
         toggleStaticTargetNavigationKey.Key.Should().Be(Keys.J, "KeyContainer.Key");
         toggleStaticTargetNavigationKey.Name.Should().Be("ToggleStaticTargetNavigation", "KeyContainer.Name");
         toggleStaticTargetNavigationKey.Shift.Should().BeFalse("KeyContainer.Shift");

         var toggleMovingTargetNavigationKey = sut.Keys.Find(x => x.Name == "ToggleMovingTargetNavigation");
         toggleMovingTargetNavigationKey.Should().NotBeNull("toggleMovingTargetNavigationKey");
         toggleMovingTargetNavigationKey.Alt.Should().BeFalse("KeyContainer.Alt");
         toggleMovingTargetNavigationKey.Ctrl.Should().BeFalse("KeyContainer.Ctrl");
         toggleMovingTargetNavigationKey.Key.Should().Be(Keys.J, "KeyContainer.Key");
         toggleMovingTargetNavigationKey.Name.Should().Be("ToggleMovingTargetNavigation", "KeyContainer.Name");
         toggleMovingTargetNavigationKey.Shift.Should().BeTrue("KeyContainer.Shift");

         var triggerLandingKey = sut.Keys.Find(x => x.Name == "TriggerLanding");
         triggerLandingKey.Should().NotBeNull("toggleDriverKey");
         triggerLandingKey.Alt.Should().BeFalse("KeyContainer.Alt");
         triggerLandingKey.Ctrl.Should().BeFalse("KeyContainer.Ctrl");
         triggerLandingKey.Key.Should().Be(Keys.L, "KeyContainer.Key");
         triggerLandingKey.Name.Should().Be("TriggerLanding", "KeyContainer.Name");
         triggerLandingKey.Shift.Should().BeFalse("KeyContainer.Shift");
         #endregion

         sut.StopOnStreet.Should().BeFalse();
         sut.Version.Should().Be(ModIdentityProvider.Identity.Version);
         typeOfError.Should().Be(ErrorType.None);
      }

      [Test, Explicit]
      public void Saves_config()
      {
         #region key container definitions

         var keys = new List<KeyContainer>
                    {
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.Left,
                          Name = "SlowDown",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = true,
                          Key = Keys.Left,
                          Name = "SlowDownFine",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.Right,
                          Name = "SpeedUp",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = true,
                          Key = Keys.Right,
                          Name = "SpeedUpFine",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.J,
                          Name = "ToggleStaticTargetNavigation",
                          Shift = false
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.J,
                          Name = "ToggleMovingTargetNavigation",
                          Shift = true
                       },
                       new KeyContainer
                       {
                          Alt = false,
                          Ctrl = false,
                          Key = Keys.L,
                          Name = "TriggerLanding",
                          Shift = false
                       }
                    };

         #endregion

         var config = new ConfigurationContainer
                      {
                         AllowRandomDriverModel = true,
                         AlwaysDriveYourself = true,
                         DriverModel = "M_Y_ROMANCAB",
                         ObeyTrafficLawsOnHighSpeed = false,
                         ObeyTrafficLawsOnLowSpeed = true,
                         Keys = keys,
                         StopOnStreet = false,
                         Version = ModIdentityProvider.Identity.Version
                      };
         // ReSharper disable once CSharpWarnings::CS0618
         ConfigurationProvider.SaveConfig(config, @"a:\AutodriveConfig.xml");
      }

      [Test]
      public void ConfigProvider_instance_created()
      {
         var sut = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity);
         sut.Should().NotBeNull();
      }

      [Test]
      public void Config_version_is_valid()
      {
         var sut = ConfigurationProvider.CreateOrGetThisFor(ModIdentityProvider.Identity);
         ErrorType typeOfError;
         sut.GetConfigurationFor<ConfigurationContainer>(ModIdentityProvider.Identity, out typeOfError);
         typeOfError.Should().Be(ErrorType.None);
      }
   }
}